//
//  ViewController.swift
//  Runkeeper Clone
//
//  Created by Christofer Berrette on 2017-11-30.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AVFoundation

class ViewController: UIViewController,  MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var hourstext: UILabel!
    @IBOutlet weak var minutestext: UILabel!
    @IBOutlet weak var secondstext: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var mapview: MKMapView!
    
    var locationManager = CLLocationManager()
    
    private var locationList: [CLLocation] = []
    
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    
    var second = 0
    var minutes = 0
    var hours = 0
    
    @objc func proccessTimer()
    {
        second += 1
        secondstext.text = "\(String(second)) S"
        if(second == 59){
            minutes += 1
            print("minutes")
            minutestext.text = "\(String(minutes)) M"
            second = 0
        }
        
        if(minutes == 60){
            hours += 1
            print("hours")
            hourstext.text = "\(String(hours)) H"
            minutes = 0
            second = 0
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapview.delegate = self
        
        var timer = Timer()
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ViewController.proccessTimer), userInfo: nil, repeats: true)
        
       
        
    }
   
    var kiloString = 1.0
   
    
    var audioPlayer: AVAudioPlayer!

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print(locations)
       
        for newLocation in locations {
            let howRecent = newLocation.timestamp.timeIntervalSinceNow
            guard newLocation.horizontalAccuracy < 20 && abs(howRecent) < 10 else { continue }
            
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
                let distanceFloat: Double = (distance).value
            
                let y = Double(round(distanceFloat))
                let km = y/Double(1000)
                let konverkm = Double(round(km*10)/10)
                
          
              
                distanceLbl.text = "\(String(konverkm)) km"
                
                if(konverkm == kiloString ) {
                    
                   
                    if let path = Bundle.main.url(forResource: "Stadium Applause-SoundBible.com-1018949101", withExtension: "mp3"){
                        do
                        {
                            let sound = try AVAudioPlayer(contentsOf: path)
                            self.audioPlayer = sound
                            sound.play()
                        }
                        catch
                        {
                            print("Error")
                        }
                    }
                    
                   
                  
                    kiloString += 1.0
                 
                 
                }
                
                
            
                
                
                let coordinates = [lastLocation.coordinate, newLocation.coordinate]
                mapview.add(MKPolyline(coordinates: coordinates, count: 2))
                let region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 500, 500)
                mapview.setRegion(region, animated: true)
            }
            
            locationList.append(newLocation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        
        return renderer
    }
}

